---
title: Project Charter
sidebar_position: 1
---

# Xcapit Project Charter

## Vision
The Xcapit project envisions a world where everyone benefits from efficient personal finance management, reaching their life goals, created by  inclusive, accessible, welcoming and open-minded communities.

## Mission
Xcapit creates an innovative service that enables software developers and community members to create solutions for everyone to learn and manage their own money, reaching their objectives and improving their wellbeing. 

## Community Statement
Xcapit is more than a crypto wallet, it is a platform built by worldwide contributors that agrees on putting the person's well-being in the center. The community is made of revolutionary people, not afraid of changing the traditional financial system. It stands for justice and transparency. We build a platform for financial freedom, no matter the user’s background or credit score.

We are focusing on accessibility, and we invite specially developers and testers with these skills to help us build it, but everyone is invited to join. No matter what your skills are, we have a place for you in our community! We are software engineers, designers and artists, system administrators, web designers, writers, speakers, translators, and more — and we are happy to help you get started.
