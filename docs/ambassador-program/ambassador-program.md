# Xcapit Ambassador Program

## **Why join our ambassador program?**

- Earn rewards. Get incentivized for your hard work in helping to grow the Xcapit Community. Incentives will be given out for completing tasks related to community growth.
- Collaborate with team members and get early access to news and releases.
- Receive awesome merchandise.
- Increase your personal network effect. Work and network by connecting with like-minded community members. Get featured on xcapit.com and social media channels.
- Access exclusive events. Have the opportunity to participate in these events with team members.
- Exclusive NFT to demonstrate your commitment as an ambassador.

## **What does an Xcapit Ambassador do?**

Ambassadors will contribute in three main areas: content creation, meet-up organization, and community channels moderation.

**Content Creator**

Community content is integral to the communication of ideas in the blockchain industry. The analysis, opinions, and perspectives generated in third-party content are part of the reason Xcapit continues to grow so dynamically. Content will be assessed based on overall quality, viewership, and engagement.

**Meet-up Organizer**

The crypto industry is about bringing people together. People who want to change the world, discuss and challenge ideas, propose solutions, learn and teach about personal finance, and meet other members of the Xcapit community are invited to become Meet-up Organizers.

**Communication Channels Moderation**

Moderators drive adoption by keeping the Xcapit community channels informative, productive, and a pleasant place for the community to come together.

## **How to apply?**

If you are interested, we invite you to fill the following [form](https://docs.google.com/forms/d/e/1FAIpQLSe7d2BGRzTDyGt3UlqsQDH4tYBk6OyyWmBgdzxB7iTWcAatjQ/viewform).
