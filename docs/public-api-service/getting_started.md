---
title: Getting Started
sidebar_position: 1
---

# Getting Started

### Installation

Clone the repository and open the directory:

```
git clone https://gitlab.com/xcapit-foss/public_api.git public-api-service
cd public-api-service
```

Ensure you have [Docker](https://www.docker.com/) and [Docker compose](https://docs.docker.com/compose/install/) installed, and [configuration files](#configuration), then:

```
docker-compose build
docker-compose up -d
```

### Making database migrations

```
docker-compose exec public_api python manage.py makemigrations
docker-compose exec public_api python manage.py migrate
```

Now in [http://localhost:9050/](http://localhost:9050/) you can see the API.

## Tests

To run the tests:

```
docker-compose exec public_api pytest
```

<h2 id="configuration">Configuration</h2>

For configuration settings you could see and change the next files.

```
./variables.env
./docker-compose.yml
```

### Example files

```
# variables.env

ENTORNO=dev
DJANGO_LOG_LEVEL=INFO
SECRET=<SECRET_KEY>

API_APP_URL=http://localhost:9070/v1/api/
API_NOTIFICATIONS=http://localhost:9051/v1/api/
ON_OFF_RAMPS_URL=http://localhost:9008/api/
API_FINANCIAL_EDUCATION=http://localhost:3000/api/
PWA_DOMAIN=<PWA_ROOT_URL>

# Hubspot
HUBSPOT_API_KEY=<HUBSPOT_API_KEY>
HUBSPOT_SUPPORT_FORM_URL=<HUBSPOT_FORM_URL>

# Influx Config
INFLUXDB_HOST=<INFLUX_HOST>
INFLUXDB_PORT=<INFLUX_PORT>
INFLUXDB_PRICE_NAME=<INFLUX_PRICE_DB_NAME>

# Sentry
SENTRY_HOST=<DJANGO_SENTRY_HOST>
SENTRY_KEY=<DJANGO_SENTRY_KEY>
```

```
# docker-compose.yml

version: "3.2"
services:
  public_api:
    build: .
    volumes:
      - .:/code
    command: python3 manage.py runserver 9050
    ports:
      - 9050:9050
    env_file:
      - ./variables.env
```

## Related Services

The Public API depends on other Xcapit services such as [Auth Service](https://xcapit-foss.gitlab.io/documentation/docs/auth-service/getting_started), [On-Off-Ramps Service](https://xcapit-foss.gitlab.io/documentation/docs/on-off-ramps-service/getting_started), [Notifications Service](https://xcapit-foss.gitlab.io/documentation/docs/notifications-service/getting_started) and Financial Education. It also interacts with third-party providers such as [Hubspot](https://www.hubspot.com/).

### Schema

The next schema represent Public API Service interaction with other services and 3rd party providers.

![Untitled](/img/public_api/service_scheme.png)
