---
title: Getting Started
sidebar_position: 1
---

# Getting Started

### Installation

Clone the repository and open the directory:

```
git clone https://gitlab.com/xcapit-foss/on-off-ramps.git on-off-ramps-service
cd on-off-ramps-service
```

Ensure you have [Docker](https://www.docker.com/) and [Docker compose](https://docs.docker.com/compose/install/) installed, and [configuration files](#configuration), then:

```
docker-compose build
docker-compose up -d
```

### Creating a database

```
docker-compose exec <DB_HOST> psql -U <DB_USER> postgres -c "CREATE DATABASE <DB_NAME>;"
```

### Making database migrations

```
docker-compose exec on-off-ramps python manage.py makemigrations
docker-compose exec on-off-ramps python manage.py migrate
```

Now in [http://localhost:9008/](http://localhost:9008/) you can see the API.

## Tests

To run the tests:

```
docker-compose exec on-off-ramps pytest
```

<h2 id="configuration">Configuration</h2>

For configuration settings you could see and change the next files.

```
./variables.env
./.env
./docker-compose.yml
```

### Example files

```
# variables.env

ENTORNO=PREPROD

# DJANGO Configuration Variables
DJANGO_SECRET_KEY=<SECRET_KEY>
DJANGO_DEBUG=False
DJANGO_LOG_LEVEL=INFO

POSTGRES_DB=<DB_NAME>
POSTGRES_USER=<DB_USER>
POSTGRES_PASSWORD=<DB_PASSWORD>
POSTGRES_HOST=<DB_HOST>
POSTGRES_PORT=<DB_PORT>

# Kripton URLs
KRIPTON_QUOTATIONS=https://app.kriptonmarket.com/public/quotations
KRIPTON_API=<KRIPTON_API_URL>
KRIPTON_TOKEN=<KRIPTON_API_KEY>

DEFAULT_REQUEST_TIMEOUT = 10

# Paxful URLs
PAXFUL_API_URL=<PAXFUL_API_URL>
PAXFUL_API_KEY=<PAXFUL_API_KEY>
PAXFUL_API_SECRET=<PAXFUL_API_SECRET>
PAXFUL_KIOSK_ID=<PAXFUL_KIOSK_ID>
PAXFUL_AFFILIATE_CODE=<PAXFUL_API_KEY>

# CELERY CONFIG
CELERY_BROKER_URL=redis://redis:6379/0
CELERY_RESULT_BACKEND=redis://redis:6379/0

# Moonpay URLs
MOONPAY_URL=<MOONPAY_API_URL>
MOONPAY_API_SECRET=<MOONPAY_API_SECRET_KEY>

# Directa URLs
DIRECTA_API_URL=<DIRECTA24_API_URL>
DIRECTA_API_KEY=<DIRECTA24_WEB_STATUS_API_KEY>
API_SIGNATURE=<DIRECTA24_DEPOSIT_API_SIGNATURE>
X_LOGIN=<DIRECTA24_DEPOSIT_API_KEY>
```

```
# .env

DJANGO_SENTRY_HOST=<DJANGO_SENTRY_HOST>
DJANGO_SENTRY_KEY=<DJANGO_SENTRY_KEY>
CELERY_SENTRY_HOST=<CELERY_SENTRY_HOST>
CELERY_SENTRY_KEY=<CELERY_SENTRY_KEY>
```

```
# docker-compose.yml

version: "3.3"

services:
  redis:
    image: redis:6.0.6-alpine
    volumes:
      - ./redis:/data

  on-off-ramps:
    build: .
    volumes:
      - .:/code
    depends_on:
      - redis
    command: python3 manage.py runserver 9008
    environment:
      - DEBUG=1
      - SENTRY_HOST=${DJANGO_SENTRY_HOST}
      - SENTRY_KEY=${DJANGO_SENTRY_KEY}
    env_file:
      - ./variables.env
    ports:
      - 9008:9008
    restart: always

  postgres-ramps:
    image: postgres:12.0
    volumes:
      - ./postgres:/var/lib/postgresql/data
    ports:
      - 5437:5437
    env_file:
      - ./variables.env

  celery-worker:
    build: .
    volumes:
      - .:/code/
    env_file:
      - ./variables.env
    environment:
      - SENTRY_HOST=${CELERY_SENTRY_HOST}
      - SENTRY_KEY=${CELERY_SENTRY_KEY}
    command: >
      bash -c "celery -A on_off_ramps purge -f
      && celery worker -A on_off_ramps.celery:app -Q regular -O fair --loglevel=INFO --autoscale=4,1 -n regular_queue"
    depends_on:
      - on-off-ramps

  celery-beat:
    build: .
    volumes:
      - .:/code/
    env_file:
      - ./variables.env
    environment:
      - SENTRY_HOST=${CELERY_SENTRY_HOST}
      - SENTRY_KEY=${CELERY_SENTRY_KEY}
    command: >
      bash -c "celery -A on_off_ramps purge -f
      && celery beat -A on_off_ramps.celery:app -l INFO"
    depends_on:
      - celery-worker

  flower:
    build: .
    volumes:
      - .:/code
    env_file:
      - variables.env
    environment:
      - SENTRY_HOST=${DJANGO_SENTRY_HOST}
      - SENTRY_KEY=${DJANGO_SENTRY_KEY}
    command: flower -A on_off_ramps.celery:app --conf=/code/on_off_ramps/flower.py --persistent=True --db=/code/flower
    ports:
      - 5555:5555
    depends_on:
      - on-off-ramps
      - celery-worker
      - celery-beat
```

## Related Services

The On-Off-Ramps service relies on third-party providers to create transactions and generate payment links.

### Schema

The next schema represent On-Off-Ramps Service interaction with other services and 3rd party providers.

![Untitled](/img/on_off_ramps/service_scheme.png)