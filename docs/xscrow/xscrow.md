---
title: Xscrow
sidebar_position: 1
---

![architecture.png](../../static/img/xscrow/xcapit-banner.png)

# Smart Contract Documentation and Architecture

This document provides an overview of the Xscrow smart contract, developed by Xcapit, and its architecture. The Xscrow contract allows deposits in the configured token, and a Lender uses that information to grant credit in fiat currency. When an address wants to withdraw their deposit, they communicate with the Lender through an Oracle to verify if the operation is permitted. If an address does not comply with their credit obligations, the Lender can initiate the execution of that deposit, and an Oracle is consulted to verify if the operation is allowed.

## Index

- [Basic information](#basic-information)
- [Architecture](#architecture)
- [Flows](#flows)
- [Security](#security)
- [Repository](#repository)
- [License](#licence)

## Basic Information

- The Xscrow contract accepts deposits in the configured token and a Lender uses that information to grant credit in fiat currency.
- When an address wants to withdraw their deposit, the Xscrow contract communicates with the Lender through an Oracle to verify if the operation is permitted.
- If an address does not comply with their credit obligations, the Lender can initiate the execution of that deposit, and an Oracle is consulted to verify if the operation is allowed.

## Architecture

![architecture.png](../../static/img/xscrow/architecture.png)

- The Xscrow contract implements Ownable and Pausable from OpenZeppelin (OZ) for better security.
- The contract uses UUPS from OZ to make the contract upgradeable. The OZ contracts and the OZ plugin for Hardhat are used for this purpose.
- The Xscrow contract interacts with a configurable Oracle, which allows for changes in the Oracle implementation based on the business needs or blockchain. For example, Chainlink is not available on the X blockchain. It is essential to note that the Oracle contract is used to verify if an address can withdraw their deposit and if a Lender can execute an address's deposit. This means that the Oracle's responses are simple true or false values associated with an address and communicated to the Xscrow contract.
- The proposed Oracle contracts (CreditOracle and ManualOracle) also implement Ownable from OZ.
- There is a check between the Xscrow and Oracle contracts to ensure that communication methods between them can only be called between them.

## Flows

### Deposit (successful)

![deposit.png](../../static/img/xscrow/deposit.png)

### Withdraw Request(successful)

![withdraw-request.png](../../static/img/xscrow/withdraw-request.png)

### Partial Withdraw Request(successful)

![partial-withdraw-request.png](../../static/img/xscrow/partial-withdraw-request.png)

### Execute Deposit (successful)

![execute-deposit.png](../../static/img/xscrow/execute-deposit.png)

### Reference

![reference.png](../../static/img/xscrow/reference.png)

## Security

The Xscrow contract has implemented several security measures to prevent attacks and ensure the safety of user funds.
* Firstly, the contract implements the Ownable and Pausable contracts from the OpenZeppelin library, which allows the contract owner to control certain functions and pause the contract in case of emergencies.
* Secondly, the contract uses the UUPS (Upgradeable) pattern provided by the OpenZeppelin library to allow for future upgrades to the contract without losing state data or disrupting user funds. Additionally, the contract uses the OpenZeppelin Hardhat plugin to manage the deployment and upgrading of the contract.
* Thirdly, the contract interacts with an Oracle to verify whether an address can withdraw their deposit or if a lender can execute the deposit of an address. The Oracle is configurable, allowing for the use of different implementations depending on the business needs or blockchain. The CreditOracle and ManualOracle contracts have been proposed as possible Oracle implementations and have also implemented the Ownable contract.
* Fourthly, the communication between the Xscrow and Oracle contracts is secured by verifying that only authorized methods can be called between them.
* Finally, the contract has been monitored using the Defender service provided by OpenZeppelin to ensure that any potential attacks are detected and responded to promptly.  

It is worth mentioning that all third-party code used in the development of the Xscrow contract and the proposed Oracles comes from reviewed and updated versions provided by OZ and Chainlink. This reduces the risk of potential security vulnerabilities due to outdated or unreviewed code.

## Repository

[https://gitlab.com/xcapit-foss/xscrow](https://gitlab.com/xcapit-foss/xscrow)

## Licence

GNU AFFERO GENERAL PUBLIC LICENSE  
Version 3, 19 November 2007

[https://gitlab.com/xcapit-foss/xscrow/-/blob/develop/LICENSE](https://gitlab.com/xcapit-foss/xscrow/-/blob/develop/LICENSE)
