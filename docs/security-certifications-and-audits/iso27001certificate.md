---
title: ISO 27001:2013 Cert
sidebar_position: 2
---

# XCAPIT DE CRYPTOLAB S.A.S. 27001 iqnet

## CERTIFICATE

IRAM has issued an IQNet recognized certificate that the organization:
XCAPIT DE CRYPTOLAB S.A.S.

**Has implemented and maintains a Information Security Management System for the following scope:**

**Design and development of the ''Xcapit Wallet'' application for access and operation with blockchains, web 3.0, metaverses, decentralized finance and cryptocurrencies.**

**Which fulfils the requirements of the following standard:**

**ISO/IEC 27001:2013
Issued on: 22/03/2022
Expires on: 22/03/2025**
This attestation is directly linked to the IQNet Partner’s original certificate and shall not be used as a stand-alone document

[XCAPIT DE CRYPTOLAB S.A.S. 27001 iqnet.pdf](../../static/XCAPIT_DE_CRYPTOLAB_S.A.S._27001_iqnet.pdf)

[XCAPIT DE CRYPTOLAB S.A.S. 27001 iram.pdf](../../static/XCAPIT_DE_CRYPTOLAB_S.A.S._27001_iram.pdf)

**Registration Number:AR - IS 39**

