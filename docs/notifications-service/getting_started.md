---
title: Getting Started
sidebar_position: 1
---

# Getting Started

### Installation

Clone the repo and open the directory:

```sh
git clone https://gitlab.com/xcapit-foss/notifications-admin notification-service
cd notification-service
```

Ensure you have [Docker](https://www.docker.com/) and [Docker compose](https://docs.docker.com/compose/install/) installed, and [configuration files](#configuration), then:

```sh
docker-compose build
docker-compose up -d
```

### Creating a database

```sh
docker-compose exec <DB_HOST> psql -U <DB_USER> -c "CREATE DATABASE <DB_NAME>;"

```

### Making database migrations

```sh
docker-compose exec notification-service python manage.py makemigrations
docker-compose exec notification-service python manage.py migrate

```

Now in [http://localhost:9001/](http://localhost:9001/) you can see the api.

## Tests

To run the tests:

```sh
docker-compose exec notification-service pytest

```

<h2 id="configuration">Configuration</h2>
For configuration settings you could see and change the next files.

```
./variables.env
./.env
./docker-compose.yml

```

### Example files

```
# variables.env

TELEGRAM_API=https://api.telegram.org
ADMINS_CHAT_GROUP_ID=<TELEGRAM_ADMIN_CHAT_GROUP_ID>
DEVS_CHAT_ID=[{'nombre': '<EXAMPLE_NAME>', 'chat_id': <TELEGRAM_CHAT_ID>}]
TG_TOKEN=<TELEGRAM_AUTHENTICATION_TOKEN>
TG_TOKEN_ADMIN=<TELEGRAM_ADMIN_AUTHENTICATION_TOKEN>

ENTORNO=dev

API_URL=http://localhost:9010/v1/api/
API_APP=http://localhost:9070/v1/api/
API_NOTIFICATIONS=http://localhost:9051/v1/api/


# Email config
EMAIL_HOST=<EMAIL_HOST>
EMAIL_PORT=465
EMAIL_HOST_USER=<EMAIL_USER>
EMAIL_HOST_PASSWORD=<EMAIL_PASSWORD>
EMAIL_USE_SSL=1
EMAIL_USE_TLS=0
EMAIL_BACKEND=django.core.mail.backends.smtp.EmailBackend
EMAIL_FROM_USER=<EMAIL_FROM_USER>

# DJANGO Configutarion Variables
DJANGO_LOG_LEVEL=INFO

# Postgres config
POSTGRES_DB=<DB_NAME>
POSTGRES_USER=<DB_USER>
POSTGRES_PASSWORD=<DB_PASSWORD>
POSTGRES_HOST=<DB_HOST>
POSTGRES_PORT=<DB_PORT>

# Celery and redis config
CELERY_BROKER_URL=redis://redis:6379/0
CELERY_RESULT_BACKEND=redis://redis:6379/0
BROKER_URL=redis://redis:6379/0

FCM_API_KEY=<FCM_API_KEY>

```

```
# .env

DJANGO_SENTRY_HOST=<DJANGO_SENTRY_HOST>
DJANGO_SENTRY_KEY=<DJANGO_SENTRY_KEY>
CELERY_SENTRY_HOST=<CELERY_SENTRY_HOST>
CELERY_SENTRY_KEY=<CELERY_SENTRY_KEY>

```

```
# docker-compose.yml

version: "3.2"
services:
  notification-service:
    build: ./wrapperbotadmin
    volumes:
      - ./wrapperbotadmin:/code
    command: python3 manage.py runserver 9001
    environment:
      - SENTRY_HOST=${DJANGO_SENTRY_HOST}
      - SENTRY_KEY=${DJANGO_SENTRY_KEY}
    ports:
      - 9001:9001
      - 9051:9051
    depends_on:
      - postgres-notification
    env_file:
      - ./wrapperbotadmin/variables.env
  
  postgres-notification:
    image: postgres:12.0
    volumes:
      - ./postgres:/var/lib/postgresql/data
    ports:
      - 5436:5436
    env_file:
      - ./wrapperbotadmin/variables.env

  redis:
    image: redis:6.0.6-alpine
    volumes:
        - ./redis/data

  celery:
    build: ./wrapperbotadmin
    volumes:
      - ./wrapperbotadmin:/code
    environment:
      - SENTRY_HOST=${CELERY_SENTRY_HOST}
      - SENTRY_KEY=${CELERY_SENTRY_KEY}
    env_file:
      - ./wrapperbotadmin/variables.env
    command: celery worker -A wrapperbotadmin.celery:app -Q default_emails_queue -O fair --loglevel=INFO --autoscale=4,1
    depends_on:
      - notification-service
      - redis

  flower:
    build: ./wrapperbotadmin
    volumes:
      - ./wrapperbotadmin:/code
    env_file:
      - ./wrapperbotadmin/variables.env
    command: flower -A wrapperbotadmin.celery:app --conf=/code/wrapperbotadmin/flower.py --persistent=True --db=/code/flower
    environment:
      - SENTRY_HOST=${CELERY_SENTRY_HOST}
      - SENTRY_KEY=${CELERY_SENTRY_KEY}
    ports:
      - 5555:5555
    depends_on:
      - notification-service
      - redis
      - celery
```

## Related Services

Xcapit Notification Service depends on mail service to send email notifications and firebase cloud messaging service to send push notifications.

### Schema

The next schema represent Xcapit Notification Service interaction with services.

![https://gitlab.com/xcapit-foss/documentation/-/raw/main/static/img/smart_wallet/XcapitSmartWallet_services_interaction.jpeg](https://gitlab.com/xcapit-foss/documentation/-/raw/main/static/img/smart_wallet/XcapitSmartWallet_services_interaction.jpeg)