﻿---
title: Xcapit Research Report
sidebar_position: 1
---

**Topic: Investment Feature & Financial Education**

Researcher: Santiago Waisman - UX Researcher

February 2022

**

**Qualitative usability test XE "Qualitative usability test"**** 

No. of participants: 6

**Participant 1** XE "**Qualitative Usability Test:**Participant 1"** 

Age: 31 to 35

City: Buenos Aires, Argentina

Date: 03/02/2022

**Task: Open investment**

The participant quickly enters the investment section, reviewing and understanding with ease the elements presented when selecting the product. 

When completing the amount to invest, he first looks at the amount in dollars and tries to complete it in that currency. Before confirming the investment, he also corroborates the dollar value by carefully reviewing the details of the transaction.

**Task: Add amount to investment**

In the initial investment section, the participant has a hard time finding the product he has already invested in. He comments that there is no indication of where to go to view his products. Once he enters the product, he quickly completes the steps to add money to his investment, commenting that it is easier than other platforms.

**Task: Withdraw amount**

He repeats the steps in the previous section to easily enter the active investment product, selecting the withdrawal button. Once on this screen, it takes a while for him to find the button to select the percentage of the investment he wants to withdraw, preferring to fill in the amount directly rather than choosing a percentage.

**Experience**

The participant considers that his experience was good, considering the functionality as easy and intuitive, with clear buttons. He also says that he would recommend this functionality to others.

















**Participant 2**

Age: 18 to 25

City: Querétaro, Mexico

Date: 4/2/2022

**Task: Open investment**

The participant intuitively navigates to the investment section, quickly and smoothly completing the steps to the screen to select the investment amount. Here he completes the amount directly in the investment currency, carefully reviewing all investment details before moving forward. He particularly stopped at the transaction fee to understand if that was the amount that the product would ultimately have, commenting it was low compared to others.

**Task: Add amount to investment**

He quickly finds the original investment, completing the necessary steps to add an amount to the investment in an intuitive way. He comments that he finds it quite easy to use.

**Task: Withdraw amount**

As in the previous section, he quickly enters his investment product, selecting the withdraw button in that section. When he reaches this screen, it takes him a while to find the button to select the percentage of the amount to withdraw. In this case he also inquires about the amount that is charged when withdrawing money to understand if it is on the profit or on the total amount withdrawn.

**Experience**

The participant comments that it was quite simple, he liked how the section works and how it simplifies the information for people who do not have technical knowledge.

He would like to include more details about the product, recommended amounts to invest and a calculation of expected returns.




















**Participant 3**

Age: 35+

City: Rosario, Argentina

Date: 3/2/2022

**Task: Open investment**

The participant quickly goes to investments, where he carefully reads the yields of the different products. As he moves to the investment product section, he comments that to be able to do the investment, he should have in pesos the equivalent amount to what appears in dollars. There he tries to fill in the investment amount in dollars and carefully reads all the details of the transaction. Then he completes the rest of the process smoothly.

**Task: Add amount to investment**

The user takes a long time to find the investment he has open, asking for help when he cannot find the product. He focuses on the other available products, following the "you might be interested" message.

Once he enters the open investment, he easily completes the steps, exclaiming that he found it super simple.

**Task: Withdraw Amount**

He easily logs into his investment product, proceeding to the withdrawal screen. When he gets to this screen, it takes him a while to find the button to select the percentage of the amount to withdraw. Then he completed the remaining steps with ease.

**Experience**

The participant finds it quite simple and easy to understand despite it being the first time.

He would like to add a warning about the percentage of his capital being invested and an additional confirmation with a summary.





















**Participant 4**

Age: 35+

City: Mexico City, Mexico

Date: 3/2/2022

**Task: Open investment**

**T**he participant simply goes to investments, where he reviews the different products and their yields. Then, when entering the product section, he comments that he is confused by the exchange rate, preferring to have information on the rate used to determine the amount in USD. He comments that the USD value should be at the bottom for information, not necessarily as an option to fill the amount.

When completing the amount and arriving at the confirmation screen, he comments that the transaction details and fee should be on the same screen where the investment amount is completed.

**Task: Add amount to investment**

When entering the investment home, he comments that his investment details should appear in USD only as an informative detail with the exchange rate, not highlighted without a reference to the rate used. When entering the product, he comments that the initial invested amount is not necessary in this part. He easily advances to the section to add money to his investment, arriving smoothly at the confirmation screen. There he comments that they should again read the terms and conditions to accept them.

**Task: Withdraw amount**

The participant easily logs in to his investment product, proceeding to the withdrawal screen. There he comments that the call to action to make a withdrawal is not clear and that the details of his investment should be able to be seen as a drop down.

The withdrawal amount or percentage should also appear with an option to fill in the number you want with a free field.

**Experience**

The participant felt that his experience was good with the open and add investment part, being simple to complete and quite clear with the exception of the exchange rate used to show the value in USD. The withdrawal part of the investment was not intuitive or easy to use.

He would like to see the exchange rate used to convert to USD, to be able to see the details of his investment as a drop down in the investment home page and to make the withdrawal button clearer and easier to use.











**Participant 5**

Age: 18 to 25

City: Mexico City, Mexico

Date: 7/2/2022

**Task: Open investment**

The participant moved quickly to the investment section, selecting the product intuitively. When filling in the investment amount he used the value in USD. He then commented that he would like to see the details of the transaction on the same page where he decides the amount and not on the next page.

**Task: Add amount to investment**

The user easily found the open investment, following the steps of the process without any problems. He commented that he found the withdrawal fee message in this section strange, he thinks it should appear only when trying to withdraw funds and not at that moment.

**Task: Withdraw amount**

The user commented that he found this task easy to perform, quickly finding the necessary buttons to select the withdrawal amount. He commented that he would like to have a free field in case he wants to select a specific percentage. 

**Experience**

The participant comments that overall he liked it quite a lot and found it simple compared to other apps, being easy and intuitive to use and with the information well organized.

He would like more information in the product details and to add a free percentage option when withdrawing the amount.























**Participant 6**

Age: 26 to 30

City: Mexico City, Mexico

Date: 8/2/2022

**Task: Open investment**

The participant easily completes the steps to the investment section, selecting the product in an intuitive way. He comments that he finds the interface intuitive and easy to use. When completing the investment amount he used the value in USD. He wants to see the product and transaction details before moving on, but it appears on the next screen, so he says he would like to see it all in one screen. He then comments that the transaction fees seem low to him.

**Task: Add amount to investment**

Due to connection problems the user did not complete this section.

**Task: Withdraw amount**

Due to connection problems the user did not complete this section.

**Experience**

The participant comments that he liked this section very much, he sees a lot of potential in it once it is launched. He clarifies that he would like to change the product details screen to give a more detailed description of the product.




**Summary**

**Task: Open investment**

- Task was completed by all users relatively quickly.
- Most users attempted to complete the amount in USD (67%). One user asked to specify the exchange rate being used.
- Half of the users prefer to see the product details on the same screen as the amount is entered.

**Task: Add amount to investment**

- Task was completed but with differences in completion time, with the exception of one user who was unable to complete the last two tasks due to technical problems.
- Users who used the first version took longer than necessary to find the open inversion. Then the prototype was modified to highlight this section and the problem stopped occurring.
- All users looked at the other existing products before entering the initial investment, analyzing the returns they offer.

**Task: Withdraw amount**

- Task was completed by all users 
- All users had difficulty finding the percentage section initially. With a modification highlighting the percentages this difficulty was reduced.
- Half of the users commented that they would prefer to have a free field to choose the percentage.

**

**Unmoderated usability testing - Maze**

For this test, three iterations of the prototype were used, with modifications that were made based on the qualitative usability tests and the results of the unmoderated tests.

The modifications that were included in the latest version are:

Enter investment amount in product currency or USD.

Modify the my investments section to highlight the users' active products.

Highlight the withdrawal percentage button in the investment withdrawal section.

The results correspond to the last iteration performed.

**Task: Open investment**

![Graphical user interface, tableDescription automatically generated with medium confidence](../../static/img/image1.png)

For this task 88.5% (23) of the users completed the task, with an average time of 61.1 seconds, averaging 5.6 seconds per screen.

Of the users who abandoned the test (3), two left on the first screen with a duration of 0 seconds, most likely due to a technical error.

Taking those users out of consideration, 95.8% of users completed the task.

From the analysis in Maze we can observe a lot of diversity in the paths used by the users, differentiating in whether or not they displayed the product details or in which currency they completed the investment amount.

65.2% of users completed the amount in rBTC.

34.8% of the users completed the amount in USD.

47.8% of users did not display the product details.

52.2% of users displayed the product details.







The screen where users spent the most time was the first screen, with 21 seconds. The error rate on this screen was 50%, showing that participants did not reach the investment button quickly, trying to click on different parts of the screen until they reached the correct button.

![](../../static/img/image4.png) 
![](../../static/img/image3.png)

Considering the evaluation of the users' experience with this task from 1 to 10, the average score was 8.5/10.

**Task: Add amount to investment**

![A picture containing tableDescription automatically generated](../../static/img/image6.png)

For this task 100% (22) of the users were successful in completing it, having an average time of 31 seconds, with 3.3 seconds on average per screen.

In this case the paths diverge according to whether they completed the amount in USD or in rBTC.

54.6% of the users completed the amount in rBTC.

45.4% of users completed the amount in USD.

For this task the screen where users stayed the longest was the first screen, where there was a 41% misclick rate. These clicks were on the buttons to invest in other products or on the menu button below that indicates that you are in the investments section, implying difficulty in finding the correct button to go to the active investments.

![](../../static/img/image5.png)
![](../../static/img/image8.png)

Considering the evaluation of the users' experience with this task from 1 to 10, the average score was 8.8/10.

**Task: Withdraw amount**

![TableDescription automatically generated with medium confidence](../../static/img/image7.png)

For this task 95.5% (21) of the users completed the task, having an average time of 20 seconds, with an average of 3.4 seconds per screen.

One person abandoned the test in this section, which, analyzing the path, seems to be an error in the navigation of the prototype. The person entered the wrong section (add amount) and could not return to the initial screen.

The screen where users spent the most time was the third screen where the withdrawal amount was selected. Here there was a misclick rate of 52%, where users tried to complete the amount in several sections, which were not available for the prototype.


![](../../static/img/image10.png)
![](../../static/img/image9.png)

Considering the evaluation of user experience with this task from 1 to 10, the average score was 7.7/10.

**NPS of the feature**

The average NPS value, understood as how likely they are to recommend this functionality to friends or family, was 7.6/10.




**Results Summary**

**Actionable Insights**

- Include product details in the same screen where the amount to be invested is filled
- Allow the completion of the amount to be invested in USD as well as the product’s token
- Create a free field to select the withdrawal percentage.
- Iterate wallet home screen to facilitate access to investment section
- Iterate investment section home to highlight open investments

These insights were applied to the latest iteration of the investment feature.

**Next Steps**

- Use in app usage analytics to validate the features usability and preferences
- Iterate the wallet home screen to make it easier for users to find the investment section
- Test the new iteration of the prototype to determine if the pain points are fixed with the new design
- Research the users’ mental models regarding the information used to make an investment decision, how they perceive risk in investments and their understanding of the products.
- Use A/B testing to determine the best option to present investment products, including the information shown to the users.

**Opportunities**

- Use information from tests and app analytics to create new investment products that fit the users’ needs.
- Use financial education feature to prepare users for the information provided in the investment feature
- Provide information for users in the investment section who wish to know more, with links to educational material
- Connect investment feature with receive, purchase and swap token flows to simplify the process when users don’t have the tokens required for the investment or the transaction fee.




**Interviews**

Nº of pariticpants: 6

**Participant 1**

Age: 31 to 35

City: Buenos Aires, Argentina

Date: 03/02/2022

**Investments**

**How do you decide to make an investment?**

He looks at the project's fundamentals when deciding. Aftera analyzing if they are good or nor he decides whether to invest.

**What things seem important to you when making an investment?** 

The rationale for the project

That the projects are backed by something

That the technology has a future, that it can last over time as a solution

**What information do you consider essential when making an investment?** 

The project’s presentation, its website, that it looks like something serious.

The activity on social networks regarding said project, the existence of a community which supports it and provides news about it, he needs to know the project has presence.

Information about the project itself, technical details and a history of the project.

**What is the biggest problem for you when making an investment?**

The price in dollars is more expensive in Argentina and the transaction fees are high.

Projects that require a lot of research before investing, since he does not have time to do that research.

Understanding how it will last in time in relation to other products.

Understanding technical issues of each product

**What do you expect from an investment platform?**

Reliable information on the products, I want to know what I’m investing in and that I can trust the platform

**What would you like to see in the investment section?**

Products with good returns and reasonable fees.

Information about the way products work and the project behind.

Analytics on the product.

**How was your experience with this feature? What did you like most about it?**

Easy and intuitive to use with clear buttons and call to action.

**If you could change anything, what would you change?** 

I would like to be able to replicate my wallet on the web

**Do you have any other comments about this feature?**

I like the different product offer and that includes many different tokens

**Financial Education**

**What do you think is the best way to learn about cryptocurrencies and managing your personal finances?** 

I prefer to do research on my own, watch tutorials or other videos on the topic.

**Where do you look for information on the subject?**

I do a lot of research from communities in social media on the topic. Youtube as well.

**What topics would you most like to learn about?** 

How to build my own blockchain project and general news on the

**Would you like to be able to learn these kinds of things from the Xcapit app?** 

If the content is interesting to me I would use it




**Participant 2**

Age: 18 to 25

City: Querétaro, Mexico

Date: 4/2/2022

**Investments**

**How do you decide to make an investment?**

He uses a spreadsheet to analyze the company's/assest’s numbers, while also analyzing the human factor, the public relations of the company or the project to determine if it is a good investment.


**What things seem important to you when making an investment?** 

The ecosystem is the most important thing, this is seen in that it can be used, for example solana allows for great possibility of transactions which makes it better than others for NFTs

**What information do you consider essential when making an investment?** 

Peaks, trends

Next movements

Evaluate the currency for one or two months

Evaluate the profitability

Project’s background


**What is the biggest problem for you when making an investment?**

Fees are too high for certain cryptocurrencies

**What do you expect from an investment platform?**

Reliable information on the value of the products, when it rises and falls. 

Having a large amount of cryptocurrencies and products  in one wallet


**What would you like to see in the investment section?**

Information on how coins move, yields and analytics to compare

**How was your experience with this feature? What did you like most about it?**

Quite simple to use, he liked the way it works.

Simplifies the information, especially for people who do not have technical knowledge

**If you could change anything, what would you change?** 

More details about the product and recommended amounts to invest and an approximate yield.

**Do you have any other comments about this feature?**

In general it was very simple to use

**Financial Education**

**What do you think is the best way to learn about cryptocurrencies and managing your personal finances?** 

He prefers doing own research through different sources, particularly reading and watching videos


**Where do you look for information on the subject?**

Youtube

Searching the internet

Reading books

He does not trust the courses on investment

**What topics would you most like to learn about?** 

How can a blockchain be applied to different purposes outside of currencies

**Would you like to be able to learn these kinds of things from the Xcapit app?** 

If it is something that brings value I would use it, but I think it would be good if they were videos or in a short format, in chapters or in blocks that are easy to go through.










**Participant 3**

Age: 35+

City: Rosario, Argentina

Date: 3/2/2022

**Investments**

**How do you decide to make an investment?**

He studies a lot before deciding to invest. He considers himself a conservative investor, so investments that promise more than 10-15% per year are risky for him.


**What things seem important to you when making an investment?** 

The yield of the product, he checks that it does not give an unreasonable or excessively risky return. 

He also considers the opinions of experts in the field.


**What information do you consider essential when making an investment?** 

Technical aspect, companies behind the currencies, support that generates confidence. Have information that is truthful


**What is the biggest problem for you when making an investment?**

Difficulties in sending money to the platforms.

**What do you expect from an investment platform?**

Reliability


**What would you like to see in the investment section?**

Reliable information on investments, tokens available, information about them

Background of the project of the token


**How was your experience with this feature? What did you like most about it?**

Quite simple, easy to understand despite being the first time.

**If you could change anything, what would you change?** 

More details on the product

**Do you have any other comments about this feature?**

Simple to understand, even for the first try

**Financial Education**

**What do you think is the best way to learn about cryptocurrencies and managing your personal finances?** 

Printed material, received by mail, newsletter style



**Where do you look for information on the subject?**

Journalists, form the internet or books.

**What topics would you most like to learn about?** 

Cryptocurrency mining

Blockchain, alternative uses

**Would you like to be able to learn these kinds of things from the Xcapit app?** 

He would find it easy and convenient to learn some essential facts.














**Participant 4**

Age: 35+

City: Mexico City, Mexico

Date: 3/2/2022

**Investments**

**How do you decide to make an investment?**

It arises from the desire to generate additional profit with money, which makes you look for ways to learn different ways that can be achieved by investing. In the case of cryptocurrencies it arises from hearing of cases where people earn a lot of money, so that makes you want to learn how it can be achieved, how it works.


**What things seem important to you when making an investment?** 

The project behind the coin is the most important thing, making sure it has strong fundamentals and a believable purpose.

I need to understand how decentralization works, in what blockchain it is based on.

It is important to have an understanding of everything behind it before deciding whether to invest or not


**What information do you consider essential when making an investment?** 

Background of the project

Growth

Peaks

Why should I invest in this coin

How many coins issued, maximum number of tokens

Main indicators that recommend this investment

**What is the biggest problem for you when making an investment?**

Lack of reliable information on some indicators

**What do you expect from an investment platform?**

Reliable information on the value of the products and attractive, reliable yields. Good user interface is also important.


**What would you like to see in the investment section?**

Details on the performance of my investments, an analytics table to review.

**How was your experience with this feature? What did you like most about it?**

With the  open investment and adding amount to investment sections, the experience was good, the withdrawal part was not intuitive for him.


**If you could change anything, what would you change?** 

Include the exchange rate being used.

Show the details of my investments in a drop-down menu.

Make the withdrawal button clearer and simpler


**Do you have any other comments about this feature?**

It could be good with some improvements

**Financial Education**

**What do you think is the best way to learn about cryptocurrencies and managing your personal finances?** 

Online, for beginners the best way is through free courses such as udemy or microtrainings.


**Where do you look for information on the subject?**

Youtube, Tik Tok, twitter

**What topics would you most like to learn about?** 

NFTs, what supports them, what is behind them, on what protocol they work

Blockchain, be able to explain more precisely what it is, how it works.

**Would you like to be able to learn these kinds of things from the Xcapit app?** 

It would be nice to have a repository that gives you information and connects you to other resources to learn.



**Participant 5**

Age: 18 to 25

City: Mexico City, Mexico

Date: 7/2/2022

**Investments**

**How do you decide to make an investment?**

Analyzes peaks, trends, growth projections, looks for information about the project in social networks.


**What things seem important to you when making an investment?** 

That the project has technical backing and support in the community.



**What information do you consider essential when making an investment?** 

Technical information about the coin (peak, growth, trends)

Who supports it

Potential application


**What is the biggest problem for you when making an investment?**

You have to have many different wallets when it comes to investing, it would be useful to have many things in one place.

**What do you expect from an investment platform?**

To give me different investment options with good information and that yields more than others.


**What would you like to see in the investment section?**

The products available, a summary of the trend over the last few months and the return.

**How was your experience with this feature? What did you like most about it?**

It was easy and intuitive to use. The information displayed is well organized


**If you could change anything, what would you change?** 

I would include the product details before selecting the amount.


**Do you have any other comments about this feature?**

Overall I liked it a lot and found it quite simple.





**Financial Education**

**What do you think is the best way to learn about cryptocurrencies and managing your personal finances?** 

Searching online, watching videos on the topic, in communities that recommend information

Short videos or blocks on different topics that can be done in a short time and in a personalized way.

**Where do you look for information on the subject?**

Youtube, tik tok, twitter

**What topics would you most like to learn about?** 

More details about NFTs, how they work and how they can be used in the metaverse.

**Would you like to be able to learn these kinds of things from the Xcapit app?** 

Yes, it would be nice to have this kind of info there.





**Participant 6**

Age: 26 to 30

City: Mexico City, Mexico

Date: 8/2/2022

**Investments**

**How do you decide to make an investment?**

Check whether it has a good project behind it. He prefers to do his own research to see if it has any backing.

Make an analysis of the project to decide whether to invest or not


**What things seem important to you when making an investment?** 

Take into account the pre-sale info on the project or the fundamentals behind a product so I can trust it has value.

Know that it has community support.


**What information do you consider essential when making an investment?** 

Background of the project

Backing from companies

Time of trajectory

Support from the community, information from this source.


**What is the biggest problem for you when making an investment?**

Not being able to keep up to date with information on the topic.

**What do you expect from an investment platform?**

Brief information on growth

Technical analysis of the product

Cryptocurrency trends

**What would you like to see in the investment section?**

Details on the performance of my investments, an analytics table to review.

**How was your experience with this feature? What did you like most about it?**

Liked it a lot, thinks it has potential once it is launched.


**If you could change anything, what would you change?** 

Would change the screen where the product details are displayed.


**Do you have any other comments about this feature?**

I think it will be useful once it is launched.

**Financial Education**

**What do you think is the best way to learn about cryptocurrencies and managing your personal finances?** 

From a community that supports that info and is active with keeping up with the topic. 

**Where do you look for information on the subject?**

Youtube

**What topics would you most like to learn about?** 

Information about the metaverse

Basic info for common people on cryptocurrencies, how blockchain works, etc. 


**Would you like to be able to learn these kinds of things from the Xcapit app?** 

He would use it, he thinks it also helps people new to investing to use the app



**Interview Summary**


**Investments**

**How do you decide to make an investment?**

All users do a lot of research on their own before deciding.

They consult many sources and make their own analysis before deciding.

The claim they have thorough and analytical mindset before their decisions


**What things seem important to you when making an investment?** 

They consider that the most important thing is the fundamentals of the project in which they are investing.

It must be profitable by analyzing financial indicators.

The projects should be backed by something

The projects should  last over time as a technology.

It is necessary to know what is behind what is invested.


**What information do you consider essential when making an investment?** 

Backing of companies

Time of trajectory

Community support

Profitability

Project background

Growth

Peaks

Why invest in this coin

How many coins issued, maximum number of tokens

Main indicators recommending this investment

Stock history/trends


**What is the biggest problem for you when making an investment?**

Not having up to date information

Having unreliable information

Analyzing technical aspects that exceed your knowledge

High costs


**What do you expect from an investment platform?**

Brief growth information

Technical analysis of the product

cryptocurrency trends

Analytics table

Having a large amount of cryptocurrencies in one wallet

Reliability


**Financial Education**

**What do you think is the best way to learn about cryptocurrencies and managing your personal finances? Where do you look for information on the subject?**

Searching online on your own

In cryptocurrency communities

Videos or short presentations

Youtube, Tik Tok, twitter

Books

Articles shared in communities



**What topics would you most like to learn about?** 

Specific knowledge of Blockchain:

Details of how it works

Uses of blockchain outside cryptocurrencies

NFTs and metaverse

\*Despite having knowledge in cryptocurrencies, everyone considers it important to have introductory content on cryptocurrencies and personal finance for those who are just starting out, as it would have helped them to have it. 



**What do you think is the best way for you to structure the financial education feature?**

The best method for users is to structure the content in blocks that can be done in a short time and can be chosen according to their interests.

One user commented that it would be good to have a progressive structure of these blocks to understand how to move forward.

